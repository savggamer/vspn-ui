import { Component, OnInit } from '@angular/core';
import {TeamService} from "../services/team.service";
import {PlayerService} from "../services/player.service";
import {PlaysService} from "../services/plays.service";
import {ScheduleService} from "../services/schedule.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private teams : TeamService, private players : PlayerService, private plays : PlaysService, private schedule : ScheduleService) { }

  ngOnInit() {
  }

}
