import { Component, OnInit } from '@angular/core';
import {Schedule} from "../../models/Schedule";
import {PlaysService} from "../services/plays.service";
import {Play} from "../../models/Play";

@Component({
  selector: 'app-plays',
  templateUrl: './plays.component.html',
  styleUrls: ['./plays.component.scss'],
  inputs: ['schedule']
})
export class PlaysComponent implements OnInit {

  schedule : Schedule;

  constructor(private playsService : PlaysService) { }

  ngOnInit() {
  }

  getPlays() : Play[]{
    let playArr = [];

    for(let play of this.playsService.plays){
      if(this.sameTeams(play) && this.sameTime(play)){
        playArr.push(play);
      }
    }

    return playArr;
  }

  private sameTime(play: Play) {
      let date = play.schedules.split(", ")[0];
      let time = play.schedules.split(", ")[1];

      return this.schedule.dates == date && this.schedule.times == parseInt(time);
  }

  private sameTeams(play: Play) {
      let home : string = this.schedule.homes;
      let away : string = this.schedule.aways;

      return (play.defender_teams == home && play.receiver_teams == away) || (play.receiver_teams == home && play.defender_teams == away);
  }

}
