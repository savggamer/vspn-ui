import {Component, OnInit} from '@angular/core';
import {Schedule} from "../../../models/Schedule";
import {ScheduleService} from "../../services/schedule.service";
import {PlayerService} from "../../services/player.service";
import {PlaysDao} from "../../../dao/PlaysDao";
import {Play} from "../../../models/Play";

@Component({
  selector: 'app-add-plays',
  templateUrl: './add-plays.component.html',
  styleUrls: ['./add-plays.component.scss']
})
export class AddPlaysComponent implements OnInit {

  selectedSchedule: Schedule;
  offense: string;
  defense: string;
  quarterback: string;
  receiver: string;
  receiverResult: {name: string, include: string[], hasDefender: boolean};
  defender: string;
  defenderResult: string;
  defensiveResults = ['Tackle', 'Sack', 'Safety', 'Interception', '1 Point', '2 Point', 'Touchdown'];
  offensiveResults = [{name: 'None', include: this.defensiveResults, hasDefender : true}, {
    name: 'Catch',
    include: ['Tackle'], hasDefender : true
  }, {
    name: 'Drop',
    include: ['Interception'], hasDefender : true
  }, {
    name: 'Miss',
    include: ['Interception'], hasDefender : true
  }, {
    name: '1 Point',
    include: [],
    hasDefender: false

  }, {
    name: '2 Point',
    include: [],
    hasDefender: false

  }, {
    name: 'Touchdown',
    include: [],
    hasDefender: false

  }];

  constructor(private scheduleService: ScheduleService, private playerService: PlayerService, private dao: PlaysDao) {
  }

  ngOnInit() {
  }

  public getSchedules() {
    return this.scheduleService.schedule;
  }

  public getOffensivePlayers(excludeQb = false) {
    let arr = ['Unknown'];

    for (let player of this.playerService.players) {
      if (player.team_names == this.offense && (!excludeQb || player.full_names != this.quarterback)) {
        arr.push(player.full_names);
      }
    }

    return arr;

  }

  public getDefensivePlayers() {
    let arr = ['Unknown'];

    for (let player of this.playerService.players) {
      if (player.team_names == this.defense) {
        arr.push(player.full_names);
      }
    }

    return arr;

  }

  public getOffensiveResults() {
    return this.offensiveResults;
  }

  public getDefensiveResults() {
    return this.receiverResult.include.concat(['None']);
  }

  submit() {
    let play = new Play();
    play.defender_results = this.defenderResult;
    play.defender_teams = this.defense;
    play.defenders = this.defender;
    play.quarterbacks = this.quarterback;
    play.receiver_results = this.receiverResult.name;
    play.receiver_teams = this.offense;
    play.receivers = this.receiver;
    play.schedules = this.selectedSchedule.dates + ", " + this.selectedSchedule.times;

    this.dao.post(play).subscribe(success => {
      this.dao.init();
      this.cancel();
    }, err => {
      alert("Failed" + err);
    })

  }

  cancel() {
    this.offense = null;
    this.defense = null;
    this.quarterback = null;
    this.receiver = null;
    this.receiverResult = null;
    this.defender = null;
    this.defenderResult = null;
  }

  setReceiverResult(result) {
    this.receiverResult = result;
    if (!result.hasDefender) {
      this.defender = 'None';
      this.defenderResult = 'None';
    }
  }

  public selectDefenderResult(result){
    if(result == 'None'){
      this.defender = 'None';
    }
    this.defenderResult = result;
  }


}
