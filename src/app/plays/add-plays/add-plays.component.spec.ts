import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPlaysComponent } from './add-plays.component';

describe('AddPlaysComponent', () => {
  let component: AddPlaysComponent;
  let fixture: ComponentFixture<AddPlaysComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPlaysComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPlaysComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
