import {CalendarEvent, EventColor} from "calendar-utils";
import {Schedule} from "../../models/Schedule";
import {ScheduleEventTeamColor} from "./ScheduleEventTeamColor";

export class ScheduleEvent implements CalendarEvent{


  constructor(schedule : Schedule, color : string, color2: string) {
    this.start = new Date(schedule.dates);
    this.title = schedule.locations + ", " + schedule.times + "pm - " + schedule.aways + " @ " + schedule.homes;
    this.color= new ScheduleEventTeamColor("#00b1fd","#00b1fd");
  }

  start: Date;
  title: string;
  color: EventColor;

}
