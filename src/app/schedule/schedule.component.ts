import {Component, OnInit, EventEmitter} from '@angular/core';
import {ScheduleService} from "../services/schedule.service";
import {CalendarEvent, MonthViewDay} from "calendar-utils";
import {ScheduleEvent} from "./ScheduleEvent";
import {TeamService} from "../services/team.service";
import {Subscription, Subject} from "rxjs";

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.scss']
})
export class ScheduleComponent implements OnInit {

  viewDate : any = Date.now();
  refresh : Subject<any> = new Subject();
  showEvents : boolean = true;
  view : string = 'month';

  constructor(private scheduleService : ScheduleService, private teamsService : TeamService) {
  }

  ngOnInit() {
  }

  dateClicked(event : { day: MonthViewDay; }){
    if(this.viewDate != event.day.date){
      this.showEvents = true;
    }
    this.viewDate = event.day.date;
    this.refresh.next();
  }

  handleEventClick(){

  }

  public getCalendarEvents(){
    let calendarEvents : ScheduleEvent[] = [];

    for(let schedule of this.scheduleService.schedule){

      calendarEvents.push(new ScheduleEvent(schedule, this.getTeamColor(schedule.aways), this.getTeamColor(schedule.homes)));
    }


    return calendarEvents;
  }

  private getTeamColor(teamName: string) {
    for(let team of this.teamsService.teams){
      if(team.team_names == teamName){
        return team.team_colors;
      }
    }

    return 'blue';
  }
}
