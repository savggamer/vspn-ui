import {EventColor} from "calendar-utils";

export class ScheduleEventTeamColor implements EventColor{

  constructor(primary: string, secondary: string) {
    this.primary = primary;
    this.secondary = secondary;
  }

  primary: string;
  secondary: string;

}
