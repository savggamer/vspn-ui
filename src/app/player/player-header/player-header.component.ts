import { Component, OnInit } from '@angular/core';
import {Player} from "../../../models/Player";

@Component({
  selector: 'app-player-header',
  templateUrl: './player-header.component.html',
  styleUrls: ['./player-header.component.scss'],
  inputs: ['player']
})
export class PlayerHeaderComponent implements OnInit {

  player : Player;

  constructor() { }

  ngOnInit() {
  }

}
