import {Component} from '@angular/core';
import {PlayersDao} from "../dao/PlayersDao";
import {TeamsDao} from "../dao/TeamsDao";
import {ScheduleDao} from "../dao/ScheduleDao";
import {PlaysDao} from "../dao/PlaysDao";
import {Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  constructor(private playersDao : PlayersDao, private router : Router, private playsDao : PlaysDao, private scheduleDao: ScheduleDao, private teamDao : TeamsDao) {
    playersDao.init();
    playsDao.init();
    scheduleDao.init();
    teamDao.init();
    if(!!this.parseQueryString()){
      router.navigate(["add"]);
    }
  }
  parseQueryString = function() {
    return window.location.href.indexOf("#add")!=-1;
  }
}
