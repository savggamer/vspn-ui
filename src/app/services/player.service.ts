import { Injectable } from '@angular/core';
import {Player} from "../../models/Player";

@Injectable()
export class PlayerService {

  private _players : Player[] = [];

  constructor() { }


  get players(): Player[] {
    return this._players;
  }

  set players(value: Player[]) {
    this._players = value;
  }
}
