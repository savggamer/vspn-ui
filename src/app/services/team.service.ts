import { Injectable } from '@angular/core';
import {Team} from "../../models/Team";

@Injectable()
export class TeamService {


  private _teams : Team[] = [];

  constructor() { }


  get teams(): Team[] {
    return this._teams;
  }

  set teams(value: Team[]) {
    this._teams = value;
  }
}
