import { Injectable } from '@angular/core';
import {Schedule} from "../../models/Schedule";

@Injectable()
export class ScheduleService {

  private _schedules : Schedule[] = [];

  constructor() { }


  get schedule(): Schedule[] {
    return this._schedules;
  }

  set schedule(value: Schedule[]) {
    this._schedules = value;
  }
}
