import { Injectable } from '@angular/core';
import {Play} from "../../models/Play";

@Injectable()
export class PlaysService {

  private _plays : Play[] = [];

  constructor() { }


  get plays(): Play[] {
    return this._plays;
  }

  set plays(value: Play[]) {
    this._plays = value;
  }
}
