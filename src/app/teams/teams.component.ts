import { Component, OnInit } from '@angular/core';
import {TeamService} from "../services/team.service";
import {Team} from "../../models/Team";
import {PlayerService} from "../services/player.service";
import {Player} from "../../models/Player";

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {

  selectedTeam : Team;

  constructor(private teamService : TeamService, private playerService : PlayerService) { }

  ngOnInit() {
  }

  getTeams(){
    return this.teamService.teams;
  }

  setSelectedTeam(team : Team){
    this.selectedTeam = team;
  }

  isSelectedTeam(team : Team){
    if(!this.selectedTeam && this.teamService.teams.length > 0){
      this.selectedTeam = this.teamService.teams[0];
    }

    return this.selectedTeam == team;
  }

  getPlayers(){
    return this.playerService.players;
  }

  isPlayerOnTeam(player : Player){
    return !!this.selectedTeam && player.team_names == this.selectedTeam.team_names;
  }

}
