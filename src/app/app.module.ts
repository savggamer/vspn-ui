import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { StatsComponent } from './stats/stats.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { PlaysComponent } from './plays/plays.component';
import { TeamsComponent } from './teams/teams.component';
import {RouterModule} from "@angular/router";
import {appRoutes} from "./app.routes";
import {ScheduleDao} from "../dao/ScheduleDao";
import {PlaysDao} from "../dao/PlaysDao";
import {PlayersDao} from "../dao/PlayersDao";
import {TeamsDao} from "../dao/TeamsDao";
import {TeamService} from "./services/team.service";
import {PlayerService} from "./services/player.service";
import {PlaysService} from "./services/plays.service";
import {ScheduleService} from "./services/schedule.service";
import { PlayerComponent } from './player/player.component';
import { PlayerHeaderComponent } from './player/player-header/player-header.component';
import {CalendarModule} from "angular-calendar";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import { AddPlaysComponent } from './plays/add-plays/add-plays.component';
import { ReversePipe } from './pipes/reverse.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    StatsComponent,
    ScheduleComponent,
    PlaysComponent,
    TeamsComponent,
    PlayerComponent,
    PlayerHeaderComponent,
    AddPlaysComponent,
    ReversePipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    CalendarModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [ScheduleDao, PlaysDao, PlayersDao, TeamsDao, TeamService, PlayerService, PlaysService, ScheduleService],
  bootstrap: [AppComponent]
})
export class AppModule { }
