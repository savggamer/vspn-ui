import {Routes} from "@angular/router";
import {TeamsComponent} from "./teams/teams.component";
import {HomeComponent} from "./home/home.component";
import {StatsComponent} from "./stats/stats.component";
import {ScheduleComponent} from "./schedule/schedule.component";
import {PlaysComponent} from "./plays/plays.component";
import {AddPlaysComponent} from "./plays/add-plays/add-plays.component";
export const appRoutes: Routes = [
  {path : 'add', component: AddPlaysComponent},
  { path: 'home', component: HomeComponent },
  { path: 'stats',      component: StatsComponent },
  { path: 'schedule',      component: ScheduleComponent },
  { path: 'teams',      component: TeamsComponent },
  { path: 'stats',      component: PlaysComponent },
  { path: '**', component: HomeComponent },
];
