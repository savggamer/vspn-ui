import {Component, OnInit} from '@angular/core';
import {Http} from "@angular/http";
import {Player} from "../../models/Player";
import {PlayerService} from "../services/player.service";
import {PlaysService} from "../services/plays.service";
import {TeamService} from "../services/team.service";
import {Team} from "../../models/Team";

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.scss']
})
export class StatsComponent implements OnInit {

  statType = 'Receiver';
  sortStat : string = 'Touchdowns';
  teamFilter = 'All';
  nameFilter ='';


  Receiver: string[] = ['Touchdowns', '2 Points', '1 Points', 'Catches', 'Drops', 'Targets'];
  Defender: string[] = ['Tackles', 'Sacks', 'Safeties', 'Interceptions', 'Pick 1', 'Pick 2', 'Pick 6'];
  Quarterback: string[] = ['Completions', 'Throws', 'Misses', 'Interceptions Thrown'];

  constructor(private http: Http, private playerService: PlayerService, private playsService: PlaysService, private teamService : TeamService) {
  }

  ngOnInit() {
  }

  public getComputedStat(statType: string, player: Player) {
    let count = 0;

    for (let play of this.playsService.plays) {
      switch (statType) {

        case 'Touchdowns':
          if (play.receiver_results == "Touchdown" && play.receivers == player.full_names) {
            count++;
          }
          break;

        case '2 Points':
          if (play.receiver_results == "2 Point" && play.receivers == player.full_names) {
            count++;
          }
          break;

        case '1 Points':
          if (play.receiver_results == "1 Point" && play.receivers == player.full_names) {
            count++;
          }
          break;

        case 'Catches':
          if ((play.receiver_results == "Catch" && play.receivers == player.full_names)
          || (play.receiver_results == "Touchdown" && play.receivers == player.full_names)
          || (play.receiver_results == "2 Point" && play.receivers == player.full_names)
          || (play.receiver_results == "1 Point" && play.receivers == player.full_names))
          {
            count++;
          }
          break;

        case 'Drops':
          if (play.receiver_results == "Drop" && play.receivers == player.full_names) {
            count++;
          }
          break;

        case 'Targets':
          if ((play.receiver_results == "Catch" && play.receivers == player.full_names)
            || (play.receiver_results == "Drop" && play.receivers == player.full_names)
            || (play.receiver_results == "Miss" && play.receivers == player.full_names)
            || (play.receiver_results == "Touchdown" && play.receivers == player.full_names)
            || (play.receiver_results == "2 Point" && play.receivers == player.full_names)
            || (play.receiver_results == "1 Point" && play.receivers == player.full_names))
          {
            count++;
          }
          break;

        case 'Tackles':
          if ((play.defender_results == "Tackle" && play.defenders == player.full_names)
            || (play.receiver_results == "Sack" && play.receivers == player.full_names)) {
            count++;
          }
          break;

        case 'Sacks':
          if ((play.defender_results == "Sack" && play.defenders == player.full_names)
            || (play.receiver_results == "Safety" && play.receivers == player.full_names)) {
            count++;
          }
          break;

        case 'Safeties':
          if (play.defender_results == "Safety" && play.defenders == player.full_names) {
            count++;
          }
          break;

        case 'Pick 1':
          if (play.defender_results == "1 Point" && play.defenders == player.full_names) {
            count++;
          }
          break;


        case 'Pick 2':
          if (play.defender_results == "2 Point" && play.defenders == player.full_names) {
            count++;
          }
          break;


        case 'Pick 6':
          if (play.defender_results == "Touchdown" && play.defenders == player.full_names) {
            count++;
          }
          break;

        case 'Completions':
          if ((play.receiver_results == "Catch" && play.quarterbacks == player.full_names)
          || (play.receiver_results == "Touchdown" && play.quarterbacks == player.full_names)
          || (play.receiver_results == "2 Point" && play.quarterbacks == player.full_names)
          || (play.receiver_results == "1 Point" && play.quarterbacks == player.full_names))
          {
            count++;
          }
          break;

        case 'Throws':
          if ((play.receiver_results == "Catch" && play.quarterbacks == player.full_names)
          || (play.receiver_results == "Touchdown" && play.quarterbacks == player.full_names)
          || (play.receiver_results == "2 Point" && play.quarterbacks == player.full_names)
          || (play.receiver_results == "1 Point" && play.quarterbacks == player.full_names)
          || (play.receiver_results == "Miss" && play.quarterbacks == player.full_names)
          || (play.receiver_results == "Drop" && play.quarterbacks == player.full_names))
          {
            count++;
          }
          break;

        case 'Misses':
          if (play.receiver_results == "Miss" && play.quarterbacks == player.full_names) {
            count++;
          }
          break;

        case 'Interceptions':
          if ((play.defender_results == "Interception" && play.defenders == player.full_names)
          || (play.defender_results == "Touchdown" && play.defenders == player.full_names)
          || (play.defender_results == "2 Point" && play.defenders == player.full_names)
          || (play.defender_results == "1 Point" && play.defenders == player.full_names))
          {
            count++;
          }
          break;

        case 'Interceptions Thrown':
          if ((play.defender_results == "Interception" && play.quarterbacks == player.full_names)
            || (play.defender_results == "Touchdown" && play.quarterbacks == player.full_names)
            || (play.defender_results == "2 Point" && play.quarterbacks == player.full_names)
            || (play.defender_results == "1 Point" && play.quarterbacks == player.full_names))
          {
            count++;
          }
          break;

      }
    }

    return count;
  }

  public getApplicableStats() {
    return this[this.statType];
  }

  public getPlayers(): Player[] {
    let filteredPlayers = this.filterPlayers();
    return filteredPlayers.sort((left : Player, right :Player)=>{
      let leftStat = this.getComputedStat(this.sortStat, left);
      let rightStat = this.getComputedStat(this.sortStat, right);
      if(rightStat - leftStat != 0){
        return rightStat - leftStat;
      }else{
        return left.full_names < right.full_names ? -1 : 1;
      }
    });
  }

  private filterPlayers() {
    let arr = [];
    for(let player of this.playerService.players){
      if(this.nameFilter.trim() == '' && this.teamFilter == 'All'){
        if(player.full_names != 'Unknown'){
          arr.push(player);
        }
      } else if(this.nameFilter.trim() == ''){
        if(player.team_names == this.teamFilter){
          if(player.full_names != 'Unknown'){
            arr.push(player);
          }
        }
      }else{
        if(player.full_names.indexOf(this.nameFilter) != -1){
          if(player.full_names != 'Unknown'){
            arr.push(player);
          }
        }
      }
    }
    return arr;
  }

  public getTeamNames() : Team[]{
    return this.teamService.teams;

  }

  public setDefaultSort(str){
    this.sortStat = this[str][0];
  }

}
