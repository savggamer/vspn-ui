export class Player{
 private _birth_dates : string;
 private _birth_places : string;
 private _full_names : string;
 private _heights : number;
 private _joined_league : string;
 private _joined_vsp : string;
 private _schools : string;
 private _team_names : string;
 private _weights : string;


  get birth_dates(): string {
    return this._birth_dates;
  }

  set birth_dates(value: string) {
    this._birth_dates = value;
  }

  get birth_places(): string {
    return this._birth_places;
  }

  set birth_places(value: string) {
    this._birth_places = value;
  }

  get full_names(): string {
    return this._full_names;
  }

  set full_names(value: string) {
    this._full_names = value;
  }

  get heights(): number {
    return this._heights;
  }

  set heights(value: number) {
    this._heights = value;
  }

  get joined_league(): string {
    return this._joined_league;
  }

  set joined_league(value: string) {
    this._joined_league = value;
  }

  get joined_vsp(): string {
    return this._joined_vsp;
  }

  set joined_vsp(value: string) {
    this._joined_vsp = value;
  }

  get schools(): string {
    return this._schools;
  }

  set schools(value: string) {
    this._schools = value;
  }

  get team_names(): string {
    return this._team_names;
  }

  set team_names(value: string) {
    this._team_names = value;
  }

  get weights(): string {
    return this._weights;
  }

  set weights(value: string) {
    this._weights = value;
  }
}
