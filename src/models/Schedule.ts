export class Schedule{
  private _aways: string;
  private _dates: string;
  private _homes :string;
  private _locations : string;
  private _seasons : string;
  private _times : number;
  private _weeks : number;


  get aways(): string {
    return this._aways;
  }

  set aways(value: string) {
    this._aways = value;
  }

  get dates(): string {
    return this._dates;
  }

  set dates(value: string) {
    this._dates = value;
  }

  get homes(): string {
    return this._homes;
  }

  set homes(value: string) {
    this._homes = value;
  }

  get locations(): string {
    return this._locations;
  }

  set locations(value: string) {
    this._locations = value;
  }

  get seasons(): string {
    return this._seasons;
  }

  set seasons(value: string) {
    this._seasons = value;
  }

  get times(): number {
    return this._times;
  }

  set times(value: number) {
    this._times = value;
  }

  get weeks(): number {
    return this._weeks;
  }

  set weeks(value: number) {
    this._weeks = value;
  }
}
