export class Play{
  private _defender_teams : string;
  private _quarterbacks : string;
  private _receiver_results : string;
  private _defender_results : string;
  private _receiver_teams : string;
  private _receivers : string;
  private _defenders : string;
  private _schedules : string;


  get defender_teams(): string {
    return this._defender_teams;
  }

  set defender_teams(value: string) {
    this._defender_teams = value;
  }

  get quarterbacks(): string {
    return this._quarterbacks;
  }

  set quarterbacks(value: string) {
    this._quarterbacks = value;
  }

  get receiver_results(): string {
    return this._receiver_results;
  }

  set receiver_results(value: string) {
    this._receiver_results = value;
  }

  get receiver_teams(): string {
    return this._receiver_teams;
  }

  set receiver_teams(value: string) {
    this._receiver_teams = value;
  }

  get receivers(): string {
    return this._receivers;
  }

  set receivers(value: string) {
    this._receivers = value;
  }

  get schedules(): string {
    return this._schedules;
  }

  set schedules(value: string) {
    this._schedules = value;
  }


  get defenders(): string {
    return this._defenders;
  }

  set defenders(value: string) {
    this._defenders = value;
  }


  get defender_results(): string {
    return this._defender_results;
  }

  set defender_results(value: string) {
    this._defender_results = value;
  }
}
