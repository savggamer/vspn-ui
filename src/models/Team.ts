export class Team{
  private _team_colors : string;
  private _team_mascots: string;
  private _team_names : string;


  get team_colors(): string {
    return this._team_colors;
  }

  set team_colors(value: string) {
    this._team_colors = value;
  }

  get team_mascots(): string {
    return this._team_mascots;
  }

  set team_mascots(value: string) {
    this._team_mascots = value;
  }

  get team_names(): string {
    return this._team_names;
  }

  set team_names(value: string) {
    this._team_names = value;
  }
}
