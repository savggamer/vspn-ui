
import {Injectable} from "@angular/core";
import {BaseDao} from "./BaseDao";
import {Http} from "@angular/http";
import {TeamService} from "../app/services/team.service";
import 'rxjs/add/operator/map';
import {Team} from "../models/Team";


@Injectable()
export class TeamsDao extends BaseDao{

  private path : string = '/teams';

  init() {
    this.http.get(this.base + this.path).map(this.extractData).subscribe(success => {
      this.teamService.teams = success;
    }, err =>{

    })
  }


  constructor(private http : Http, private teamService : TeamService) {    super();

  }
}
