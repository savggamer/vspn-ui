import {Injectable} from "@angular/core";
import {BaseDao} from "./BaseDao";
import {Http} from "@angular/http";
import {ScheduleService} from "../app/services/schedule.service";
@Injectable()
export class ScheduleDao extends BaseDao{

  private path : string = '/schedules';

  init() {
    this.http.get(this.base + this.path).map(this.extractData).subscribe(success => {
      this.scheduleService.schedule = success;
    }, err =>{

    })
  }

  constructor(private http : Http, private scheduleService : ScheduleService) {    super();

  }
}
