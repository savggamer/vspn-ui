

import {Response} from "@angular/http";

export abstract class BaseDao{
  protected base : string = "https://flag-football-stat-tracker.herokuapp.com/api";


  constructor() {
  }

  public abstract init();

  extractData(res : Response){
    return res.json();
  }
}
