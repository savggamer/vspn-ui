import {BaseDao} from "./BaseDao";
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {PlayerService} from "../app/services/player.service";
import {PlaysService} from "../app/services/plays.service";
import {Play} from "../models/Play";

@Injectable()
export class PlaysDao extends BaseDao{
  private path : string = '/plays';

  init() {
    this.http.get(this.base + this.path).map(this.extractData).subscribe(success => {
      this.playsService.plays = success;
    }, err =>{

    })
  }


  constructor(private http : Http, private playsService : PlaysService) {    super();

  }

  post(play : Play){


    return this.http.post(this.base + this.path, {defender_teams:play.defender_teams, defenders: play.defenders, quarterbacks: play.quarterbacks, receiver_results:play.receiver_results, receiver_teams : play.receiver_teams, receivers : play.receivers, schedules : play.schedules, defender_results : play.defender_results}).map(this.extractData);
  }
}
