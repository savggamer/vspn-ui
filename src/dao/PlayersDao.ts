import {BaseDao} from "./BaseDao";
import {Injectable} from "@angular/core";
import {Http} from "@angular/http";
import {PlayerService} from "../app/services/player.service";

@Injectable()
export class PlayersDao extends BaseDao{
  private path : string = '/players';
  init() {
    this.http.get(this.base + this.path).map(this.extractData).subscribe(success => {
      this.playerService.players = success;
    }, err =>{

    })
  }

  constructor(private http : Http, private playerService : PlayerService){
    super();
  }

}
