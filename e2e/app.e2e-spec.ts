import { VspnUiPage } from './app.po';

describe('vspn-ui App', () => {
  let page: VspnUiPage;

  beforeEach(() => {
    page = new VspnUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
